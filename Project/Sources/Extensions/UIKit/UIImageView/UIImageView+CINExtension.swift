//
//  UIImageView+extension.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 30.10.2021.
//

import UIKit
import Kingfisher
import SkeletonView

extension CINExtensions where Base == UIImageView {
    
    struct CustomIndicator: Indicator {
        
        let view = UIView()
        let loaderView = UIImageView(image: R.image.imgLoaderOnImg())
        
        public var centerOffset: CGPoint {
            .init(x: view.center.x - loaderView.frame.width / 2, y: view.center.y - loaderView.frame.height / 2)
        }
        
        init() {
            view.backgroundColor = .clear
            loaderView.frame = CGRect(origin: view.center, size: CGSize(width: 36, height: 36))
            view.addSubview(loaderView)
        }
        
        func startAnimatingView() {
            view.isHidden = false
            loaderView.cin.rotate()
        }
        
        func stopAnimatingView() {
            view.isHidden = true
            loaderView.cin.stopRotating()
        }
    }
    
    @discardableResult
    func setImage(
        fromURLString urlString: String?,
        withPlaceholder placeholder: UIImage? = UIImage.cin.create(withColor: .clear),
        imageSize: CGSize? = nil,
        needResize: Bool = false,
        animated: Bool = false,
        loader: Bool = false,
        specialLoaderContainerFrame: CGRect? = nil,
        showSkeleton: Bool = false,
        failureImage: UIImage? = R.image.imgStub(),
        completion: ((_ image: UIImage?, _ success: Bool) -> Void)? = nil
    ) -> DownloadTask? {
        self.base.backgroundColor = R.color.backgroundCardOnPhoto()
        
        var kf = self.base.kf
        let customIndicator = CustomIndicator()
        if let specialLoaderContainerFrame = specialLoaderContainerFrame {
            customIndicator.view.frame = specialLoaderContainerFrame
        } else {
            customIndicator.view.frame = self.base.frame
        }
        
        kf.indicatorType = loader ? .custom(indicator: customIndicator) : .none
        
        guard let url = URL(string: urlString?.cin.encodedUrl ?? "") else {
            self.base.backgroundColor = .clear
            completion?(nil, false)
            self.base.image = failureImage
            return nil
        }

        var options = [
            KingfisherOptionsInfoItem.cacheOriginalImage,
            KingfisherOptionsInfoItem.callbackQueue(.mainAsync)
        ]
        
        var cacheKey = url.absoluteString

        if needResize {
            var size = imageSize ?? self.base.frame.size
            if self.base.frame.size == .zero {
                size = CGSize(width: 50, height: 50)
            }
            
            let scale = UIScreen.main.scale
            let refSize = CGSize(width: size.width * scale, height: size.height * scale)

            let resizingProcessor = ResizingImageProcessor(referenceSize: refSize,
                                                           mode: .aspectFill)
            options.append(.processor(resizingProcessor))

            cacheKey = "\(cacheKey)_\(refSize.width)_\(refSize.height)"
        }

        let resource = ImageResource(downloadURL: url,
                                     cacheKey: cacheKey)
        if animated {
            options.append(.transition(ImageTransition.fade(0.3)))
        }

        if showSkeleton && !ImageCache.default.isCached(forKey: cacheKey) {
            self.base.showAnimatedSkeleton()
        }
        
        let imageView = self.base
        
        return self.base.kf.setImage(
            with: resource,
            placeholder: placeholder,
            options: options,
            progressBlock: { (_, _) in },
            completionHandler: { [weak imageView] (result) in
                if showSkeleton { imageView?.hideSkeleton() }
                
                switch result {
                case .failure(let error):
                    if error.isNotCurrentTask { return }
                    
                    self.base.backgroundColor = .clear
                    completion?(nil, false)
                    imageView?.image = failureImage
                                        
                default:
                    self.base.backgroundColor = .clear
                    completion?(nil, true)
                }
            })
    }
}
