//
//  UIImage+extension.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 31.10.2021.
//

import UIKit

public extension CINExtensions where Base: UIImage {

/// Image's width
    var width: CGFloat {
        self.base.size.width
    }

/// Image's height
    var height: CGFloat {
        self.base.size.width
    }

/// Create image with full color fill
///
/// - Parameters:
///   - color: color for fill image. `UIColor`
///   - rect: result image frame. `CGRect`
///
/// - Returns: filled with color image
    static func create(withColor color: UIColor?,
                       rect: CGRect = CGRect(x: 0, y: 0, width: 1, height: 1)) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        color?.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return image
    }
}
