//
//  UIView+CINExtension.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import UIKit

public extension CINExtensions where Base: UIView {
    
    static var kRotationAnimationKey: String { "rotationanimationkey" }
    
/// View's nib, based on view's class name
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle: Bundle(for: Base.self))
    }

/// Create view's example
///
/// - Precondition: View must have .xib-file and that name must be equal to view's class
/// - Returns: example of view
    static func instance() -> Base {
        if let nib = UINib(nibName: self.identifier,
                            bundle: nil)
            .instantiate(withOwner: self, options: nil).first as? Base {
            
            return nib
        }
        
        fatalError("not found nib file!")
    }
    
/// Load view from that's nib
///
/// - Returns: `UIView`
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self.base))
        if let nibName = type(of: self.base).description().components(separatedBy: ".").last {
            let nib = UINib(nibName: nibName, bundle: bundle)
            if let view = nib.instantiate(withOwner: self, options: nil).first as? UIView {
                return view
            }
        }
        return UIView()
    }
    
/// Corner radius of view
    var cornerRadius: CGFloat {
        get { self.base.layer.cornerRadius }
        set {
            self.base.layer.cornerRadius = newValue
            self.base.clipsToBounds = true
        }
    }

/// Blink view with alpla
///
/// - Parameters:
///   - alpha: alpha value. `CGFloat`, by default - `0.2`
///   - duration: blink animation's duration. `TimeInterval`, by default - `0.3`
    func blink(withAlpla alpha: CGFloat = 0.2,
               duration: TimeInterval = 0.3) {
        self.base.alpha = alpha
        UIView.animate(withDuration: duration) {
            self.base.alpha = 1.0
        }
    }
    
/// Rotate view with duration
    func rotate(duration: Double = 1) {
        DispatchQueue.main.async {
            if self.base.layer.animation(forKey: CINExtensions<UIView>.kRotationAnimationKey) == nil {
                let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
                self.base.layer.opacity = 1
                rotationAnimation.fromValue = 0.0
                rotationAnimation.toValue = Float.pi * 2.0
                rotationAnimation.duration = duration
                rotationAnimation.repeatCount = Float.infinity
                
                self.base.layer.add(rotationAnimation, forKey: CINExtensions<UIView>.kRotationAnimationKey)
            }
            
            self.base.setNeedsLayout()
        }
    }
    
/// Stop rotating view
    func stopRotating() {
        DispatchQueue.main.async {
            if self.base.layer.animation(forKey: CINExtensions<UIView>.kRotationAnimationKey) != nil {
                self.base.layer.opacity = .zero
                self.base.layer.removeAnimation(forKey: CINExtensions<UIView>.kRotationAnimationKey)
            }
            
            self.base.setNeedsLayout()
        }
    }
}
    

