//
//  SwiftyJSON+CINDevKit.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import Foundation
import SwiftyJSON

public extension CINExtensions where Base == JSON {
    
// MARK: - Identifier
/// Returns string identifier in JSON of passed field
///
/// - Parameters:
///   - field: field for getting identifier.`String`. Default value == `id`
/// - Returns: string identifier. `String`
    func identifier(fromField field: String = "id") -> String? {
        if let stringValue = self.base[field].string { return stringValue }
        
        if let intValue = self.base[field].int { return "\(intValue)" }
        
        return nil
    }
}
