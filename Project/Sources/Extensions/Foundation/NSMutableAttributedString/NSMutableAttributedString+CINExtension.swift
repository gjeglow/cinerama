//
//  NSMutableAttributedString+CINExtension.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 31.10.2021.
//

import Foundation
import UIKit

extension CINExtensions where Base == NSMutableAttributedString {

    func setColorForText(textForAttribute: String,
                         withColor color: UIColor?,
                         withFont font: UIFont?) {
        let range: NSRange = self.base.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        let attrs = [NSAttributedString.Key.font : font ?? UIFont.systemFont(ofSize: 14),
                     NSAttributedString.Key.foregroundColor : color ?? UIColor.black]
        self.base.addAttributes(attrs, range: range)
    }
}
