//
//  NSObject+CINExtension.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 29.10.2021.
//

import Foundation

public extension CINExtensions where Base: NSObject {

/// String identifier of object
    static var identifier: String {
        return String(describing: Base.self.classForCoder())
    }
}
