//
//  String+CINExtensions.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 31.10.2021.
//

import Foundation

extension CINExtensions where Base == NSString {

    var encodedUrl: String {
        return self.base.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? ""
    }
}
