//
//  CINExtensions.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

/**
 Use `CINExtensions` proxy as customization point for constrained protocol extensions.

 General pattern would be:

 // 1. Extend CINExtensions protocol with constrain on Base
 // Read as: CINExtensions Extension where Base is a SomeType
 extension CINExtensions where Base: SomeType {
 // 2. Put any specific reactive extension for SomeType here
 }

 With this approach we can have more specialized methods and properties using
 `Base` and not just specialized on common base type.

 */

public struct CINExtensions<Base> {
/// Base object to extend.
    public var base: Base

/// Creates extensions with base object.
///
/// - parameter base: Base object.
    public init(_ base: Base) {
        self.base = base
    }
}

/// A type that has CIN extensions.
public protocol CINExtensionsCompatible {
/// Extended type
    associatedtype CINExtensionsBase
    
/// CIN extensions.
    static var cin: CINExtensions<CINExtensionsBase>.Type { get set }

/// CIN extensions.
    var cin: CINExtensions<CINExtensionsBase> { get set }
}

extension CINExtensionsCompatible {
/// CIN extensions.
    public static var cin: CINExtensions<Self>.Type {
        get {
            return CINExtensions<Self>.self
        }
        set {
        }
    }

/// CIN extensions.
    public var cin: CINExtensions<Self> {
        get {
            return CINExtensions(self)
        }
        set {
        }
    }
}

import class Foundation.NSObject

/// Extend NSObject with `cin` proxy.
extension NSObject: CINExtensionsCompatible { }

import SwiftyJSON
/// Extend JSON with `cin` proxy.
extension JSON: CINExtensionsCompatible { }
