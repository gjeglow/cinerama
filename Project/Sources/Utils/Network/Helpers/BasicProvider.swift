//
//  BasicProvider.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import Foundation
import Moya

// MARK: - HTTPHeaderField
enum HTTPHeaderField: String {

    case acceptType  = "Accept"
    case contentType = "Content-Type"
}

// MARK: - ContentType
enum ContentType: String {

    case json = "application/json"
    case jpeg = "image/jpeg"
}

// MARK: - TargetType
extension TargetType {
    
    private var baseURLString: String {
        return "https://s3-eu-west-1.amazonaws.com"
    }
    
    var baseURL: URL {
        return URL(string: self.baseURLString)!
    }
    
    var sampleData: Data {
        return Data()
    }

    var headers: [String: String]? {
        var headers = [
            HTTPHeaderField.acceptType.rawValue: ContentType.json.rawValue
        ]

        switch self.method {
        case .post, .put:
            headers[HTTPHeaderField.contentType.rawValue] = ContentType.json.rawValue

        default:
            break
        }
        
        return headers
    }
    
}
