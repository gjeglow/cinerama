//
//  NetworkHelper.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import Foundation
import Moya
import SwiftyJSON

// MARK: - NetworkHelper
class NetworkHelper {

    static let requestCancelledCode = "request_cancelled"
    
    static let config = NetworkLoggerPlugin.Configuration(
      logOptions: .default
    )
    
    static var plugins: [PluginType] = [
        NetworkLoggerPlugin(
            configuration: .init(formatter: .init(),
                                 output: { (_, array) in
                                    if let log = array.first {
                                        print(log)
                                    }
                                 }, logOptions: .formatRequestAscURL))
    ]

}

// MARK: - MoyaProvider
extension MoyaProvider {
    
    @discardableResult
    func cin_executeJSONRequest(_ target: Target,
                               completion: (@escaping (_ success: Bool, _ data: JSON?, _ message: String?, _ errorCode: String?) -> Void)) -> Cancellable {
        let stamp = Date().timeIntervalSince1970
        let cancelable = self.request(target) { (result) in
            switch result {
            case .success(let response):
                if response.statusCode == 401 {
                    completion(false, nil, nil, .networkUnauthorized)
                    return
                }

                do {
                    let json = JSON(try response.mapJSON())
                    
                    let newStamp = Date().timeIntervalSince1970
                    print("Request time: \(target.path) \(newStamp - stamp)")
                    
                    completion(true, json, nil, nil)

                } catch {
                    completion(false, nil, R.string.network.unrecognizedResponse(), nil)
                }
                
            case .failure(let moyaError):
                var errorMessage: String?
                var errorCode: String?
                switch moyaError {
                case .underlying(let underlyingError, _):
                    if let afError = underlyingError.asAFError {
                        if let error = afError.underlyingError as NSError? {
                            if error.code == NSURLErrorTimedOut {
                                errorMessage = R.string.network.slowNetworkConnection()
                            } else if error.code == NSURLErrorCannotFindHost ||
                                      error.code == NSURLErrorNetworkConnectionLost ||
                                      error.code == NSURLErrorNotConnectedToInternet ||
                                      error.code == NSURLErrorCannotConnectToHost ||
                                      error.code == NSURLErrorDataNotAllowed {
                                errorMessage = R.string.network.noNetworkConnection()
                            } else {
                                errorMessage = R.string.network.unrecognizedError()
                            }
                        } else if afError.isExplicitlyCancelledError {
                            errorCode = NetworkHelper.requestCancelledCode
                        }
                    } else {
                        errorMessage = R.string.network.unrecognizedError()
                    }
                    
                default:
                    errorMessage = R.string.network.unrecognizedError()
                }
                
                completion(false, nil, errorMessage, errorCode)
            }
        }
        
        return cancelable
    }
}

// MARK: - Common error codes
extension String {

    static let networkUnauthorized = "unauthorized"
}
