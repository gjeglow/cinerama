//
//  FilmsProvider.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 27.10.2021.
//

import Moya

enum FilmsProvider {
    
    case loadFilms
}

extension FilmsProvider: TargetType {
    
    var path: String {
        switch self {
        case .loadFilms:
            return "/sequeniatesttask/films.json"
        }
    }
    
    var method: Method {
        switch self {
        case .loadFilms:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .loadFilms:
            return .requestPlain
        }
    }
}
