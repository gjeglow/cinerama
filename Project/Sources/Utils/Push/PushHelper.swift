//
//  PushHelper.swift
//  Cinerama
//
//  Created by Gleb on 02.11.2021.
//

import UIKit
import UserNotifications
import UserNotificationsUI
import FirebaseMessaging

class PushHelper: NSObject {

    static let instance = PushHelper()

    public func configure() {
        
        Messaging.messaging().isAutoInitEnabled = true
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
    }

    public func requestPushNotificationsPermission() {

        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { [weak self] (granted, _) in
            if !granted { return }

            self?.fetchNotificationsSettings()
        }
    }

    public func loadNotificationSettings(completionHandler: @escaping (UNNotificationSettings) -> Void) {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            DispatchQueue.main.async {
                completionHandler(settings)
            }
        }
    }

    private func fetchNotificationsSettings() {
        self.loadNotificationSettings { settings in
            if settings.authorizationStatus != .authorized { return }

            UIApplication.shared.registerForRemoteNotifications()
        }
    }

}

// MARK: - UNUserNotificationCenterDelegate
extension PushHelper: UNUserNotificationCenterDelegate {

    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification,
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void
    ) {
        completionHandler([.alert, .badge, .sound])
    }
}

// MARK: - MessagingDelegate
extension PushHelper: MessagingDelegate {

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        guard let token = fcmToken else { return }
        
        print("fcmToken = \(token)")
        
    }
}
