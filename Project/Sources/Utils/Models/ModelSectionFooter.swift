//
//  ModelSectionFooter.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import UIKit

class ModelSectionFooter {

    public var title: String?
    
    func copy() -> ModelSectionFooter {
        let copy = ModelSectionFooter()
        copy.title = self.title
        return copy
    }
}
