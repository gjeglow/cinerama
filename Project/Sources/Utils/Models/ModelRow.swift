//
//  ModelRow.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import UIKit

class ModelRow {
    
    public var id: String!
    public var showSkeleton = false
    
    init() {
        self.id = UUID().uuidString
    }
    
    required init(id: String) {
        self.id = id
    }
    
    func copy() -> ModelRow {
        let copy = ModelRow()
        copy.id = self.id
        copy.showSkeleton = self.showSkeleton
        return copy
    }
}
