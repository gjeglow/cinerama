//
//  ModelTableFabric.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import UIKit

class ModelTableFabric {

    public weak var tableView: UITableView!
    
    public init(tableView: UITableView) {
        self.tableView = tableView
        self.registerCells()
    }
    
    public func registerCells() {
        self.tableView.cin.register(UITableViewCell.self)
    }
    
    public func cell(forModelRow modelRow: ModelRow,
                     atIndexPath indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    public func header(forModelSection modelSection: ModelSectionHeader?,
                     atSection section: Int) -> UIView? {
       guard modelSection != nil else { return nil }

       return UIView()
    }

    public func footer(forModelSection modelSection: ModelSectionFooter?,
                     atSection section: Int) -> UIView? {
       guard modelSection != nil else { return nil }

       return UIView()
    }
}
