//
//  ModelSectionHeader.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import UIKit

class ModelSectionHeader {
    
    public private(set) var id = UUID().uuidString
    public var expanded = true
    public var title: String?

    required init(withId id: String) {
        self.id = id
    }

    func copy() -> ModelSectionHeader {
        let copy = ModelSectionHeader(withId: self.id)
        copy.expanded = self.expanded
        copy.title = self.title
        return copy
    }
}
