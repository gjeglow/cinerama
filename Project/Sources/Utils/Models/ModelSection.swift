//
//  ModelSection.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import UIKit

class ModelSection {

    public var id: String = ""
    
    public var rows: [ModelRow] = []
    
    public var headerModel: ModelSectionHeader?
    
    public var footerModel: ModelSectionFooter?
    
    init() {
        
    }
    
    init(id: String = UUID().uuidString) {
        self.id = id
    }
    
    public func getRow(byId id: String) -> ModelRow? {
        return self.rows.filter { $0.id == id }.first
    }
    
    public func removeRow(byId id: String, index: Int? = nil) {
        self.rows.removeAll { $0.id == id }
    }
    
    func copy() -> ModelSection {
        let modelSection = ModelSection(id: self.id)
        modelSection.headerModel = self.headerModel?.copy()
        modelSection.footerModel = self.footerModel?.copy()
        modelSection.rows = rows.map{ $0.copy() }
        return modelSection
    }
    
}
