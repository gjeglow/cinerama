//
//  AppDelegate.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 27.10.2021.
//

import UIKit
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
// MARK: - Properties
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        
        let emptyCache = URLCache(memoryCapacity: 0, diskCapacity: 0, diskPath: nil)
        URLCache.shared = emptyCache
        
        self.setInitialViewController()
        self.configureCloudMessaging()
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        self.requestAppTrackingTransparency()
    }
    
// MARK: - Set Initial View Controller
    private func setInitialViewController() {
        let controller = MainViewController.cin.createWithNavigationController() 
        self.window?.rootViewController = controller
        self.window?.makeKey()
    }
    
// MARK: - App Tracking Transparency
    private func requestAppTrackingTransparency() {
        AppAnalytics.instance.requestAppTrackingTransparency()
    }

// MARK: - Cloud Messaging
    private func configureCloudMessaging() {
        PushHelper.instance.configure()
        PushHelper.instance.requestPushNotificationsPermission()
    }
}

