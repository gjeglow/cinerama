//
//  AppAnalytics.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 02.11.2021.
//

import Foundation
import AppTrackingTransparency

final class AppAnalytics {

// MARK: - Instance
    static let instance = AppAnalytics()
}

// MARK: - App Tracking Transparency
extension AppAnalytics {

    func requestAppTrackingTransparency() {
        if #available(iOS 14.0, *) {
            ATTrackingManager.requestTrackingAuthorization { _ in }
        }
    }
}
