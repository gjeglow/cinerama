//
//  MainPresenter.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 29.10.2021.
//

import Foundation

// MARK: - MainPresenter
protocol MainPresenter: BasePresenter {

// MARK: - Actions
    func onRefreshControlChanged()
    func onFilmModelClicked(_ filmId: String?)
}

// MARK: - MainPresenterImpl
class MainPresenterImpl: BasePresenterImpl {

// MARK: - VUPER
    private weak var view: MainView?
    private var router: MainRouter?

// MARK: - Data
    private var data = MainData()

// MARK: - UseCases
    private var useCases: MainUseCases

// MARK: - Init
    required init(view: MainView,
                  router: MainRouter,
                  useCases: MainUseCases) {
        self.view = view
        self.router = router
        self.useCases = useCases
    }

// MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view?.show(data: self.data)
        self.loadFilms()
    }
}

// MARK: - MainPresenter
extension MainPresenterImpl: MainPresenter {
    
    func onFilmModelClicked(_ filmId: String?) {
        guard let filmId = filmId,
              let film = self.data.films.first(where: { $0.id == filmId }) else { return }

        self.router?.showFilmCard(withFilm: film)
    }

// MARK: - OnRefreshControlChanged
    func onRefreshControlChanged() {
        if self.data.isRefreshAllowed {
            self.data.films = [Film]()
            self.loadFilms()
        }
    }
    
// MARK: - LoadBlocks
    private func loadFilms() {
        self.data.isRefreshAllowed = false
        self.view?.setLoadingVisible(true)
        
        self.useCases.listFilmsUseCase.execute { (success, films, message, _) in
            self.data.isRefreshAllowed = true
            self.view?.setLoadingVisible(false)
            
            if !success {
                self.view?.show(data: self.data)
                self.view?.showErrorMessage(message, retryBlock: {
                    self.data.films = [Film]()
                    self.loadFilms()
                })
                return
            }
            
            let sortedFilms = films.sorted(by: { $0.year ?? 0 > $1.year ?? 0 })
            self.data.films.append(contentsOf: sortedFilms)
            self.view?.show(data: self.data)
        }
    }
}
