//
//  MainData.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 29.10.2021.
//

import Foundation

// MARK: - MainData
class MainData {

// MARK: - Properties
    var films: [Film] = [Film]()
    var isRefreshAllowed = false
}
