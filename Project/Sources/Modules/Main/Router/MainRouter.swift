//
//  MainRouter.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 29.10.2021.
//

import UIKit

// MARK: - MainRouter
protocol MainRouter: BaseRouter {

    func showFilmCard(withFilm film: Film)
}

// MARK: - MainRouterImpl
class MainRouterImpl: BaseRouterImpl {

// MARK: - VUPER
    private weak var view: MainViewController?

// MARK: - Init
    required init(view: MainViewController) {
        self.view = view
    }
}

// MARK: - MainRouter
extension MainRouterImpl: MainRouter {

    func showFilmCard(withFilm film: Film) {
        self.view?.cin.push(FilmCardViewController.self, configureBlock: { (controller) in
            controller.configurator?.setFilm(film)
        })
    }
}
