//
//  MainConfigurator.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 29.10.2021.
//

import UIKit

// MARK: - MainConfigurator
protocol MainConfigurator: BaseConfigurator {

}

// MARK: - MainConfiguratorImpl
class MainConfiguratorImpl: BaseConfiguratorImpl {

// MARK: - VUPER
    private weak var presenter: MainPresenter?

// MARK: - Outlets
    @IBOutlet private weak var viewController: MainViewController!
    
// MARK: - Configure
    override func configure() {
        let router = MainRouterImpl(view: self.viewController)

        let useCases = MainUseCases(listFilmsUseCase: ListFilmsUseCaseImpl())

        let presenter = MainPresenterImpl(view: self.viewController, router: router, useCases: useCases)
        self.presenter = presenter

        self.viewController.setup(presenter: self.presenter, configurator: self)
    }
}

extension MainConfiguratorImpl: MainConfigurator {

}
