//
//  YearCell.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 29.10.2021.
//

import UIKit
import SkeletonView

class YearCell: UITableViewCell {
    
// MARK: - Properties
    private var model: YearModelRow?
    
// MARK: - Outlets
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var yearSkeletonView: UIView!
    @IBOutlet private weak var yearLabel: UILabel!
    
    // MARK: - awakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.yearLabel.font = R.font.graphikLCGMedium(size: .yearLabelFontSize)
        self.containerView.cin.cornerRadius = .containerCornerRadius
        self.yearLabel.textColor = R.color.textMain()
    }
    
// MARK: - Bind and configurate
    @discardableResult
    func bind(withModel model: YearModelRow?) -> Self {
        self.model = model
        if self.showSkeleton() { return self }
        
        self.configure()
        return self
    }
    
    private func configure() {
        self.yearLabel.text = self.model?.year?.description
    }
    
// MARK: - Private Setup
    private func showSkeleton() -> Bool {
        if self.model?.showSkeleton ?? false {
            self.containerView.backgroundColor = .clear
            self.yearSkeletonView.isHidden = false
            self.yearSkeletonView.showAnimatedSkeleton()
            return true
        }
        
        self.containerView.backgroundColor = R.color.backgroundYear()
        self.yearSkeletonView.isHidden = true
        self.yearSkeletonView.hideSkeleton()
        return false
    }
    
    
    
    override func prepareForReuse() {
        self.yearLabel.text = nil
    }
}

// MARK: - CGFloat
fileprivate extension CGFloat {

    static var yearLabelFontSize: CGFloat = 20.0
    static var containerCornerRadius: CGFloat = 6.0
}
