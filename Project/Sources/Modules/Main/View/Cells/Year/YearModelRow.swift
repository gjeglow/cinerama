//
//  YearModelRow.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 29.10.2021.
//

import Foundation

class YearModelRow: ModelRow {
    
    var year: Int?
    
    convenience init(withId id: String,
                     year: Int?) {
        self.init(id: id)
        
        self.year = year
    }
}
