//
//  FilmInfoCell.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 29.10.2021.
//

import UIKit
import SkeletonView

class FilmInfoCell: UITableViewCell {
    
// MARK: - Properties
    private var model: FilmInfoModelRow?
    
// MARK: - Outlets
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var localizedNameLabel: UILabel!
    @IBOutlet private weak var ratingLabel: UILabel!
    @IBOutlet private weak var containerView: UIView!
    
    @IBOutlet private weak var localizedNameSkeletonView: UIView!
    @IBOutlet private weak var ratingSkeletonView: UIView!
    @IBOutlet private weak var nameSkeletonView: UIView!
    
    // MARK: - awakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.containerView.cin.cornerRadius = .containerCornerRadius
        self.nameLabel.font = R.font.graphikLCGRegular(size: .nameLabelFontSize)
        self.localizedNameLabel.font = R.font.graphikLCGRegular(size: .localizedNameLabelFontSize)
    }
    
// MARK: - Bind and configurate
    @discardableResult
    func bind(withModel model: FilmInfoModelRow?) -> Self {
        self.model = model
        if self.showSkeleton() { return self }
        
        self.configure()
        self.configureRating()
        return self
    }
    
    private func configure() {
        self.nameLabel.text = self.model?.name
        self.localizedNameLabel.text = self.model?.localizedName
    }
    
    override func prepareForReuse() {
        self.nameLabel.text = nil
        self.localizedNameLabel.text = nil
        self.ratingLabel.text = nil
    }
    
    private func configureRating() {
        guard let rating = self.model?.rating?.description else {
            self.ratingLabel.font = R.font.graphikLCGRegular(size: .attributeFontSize)
            self.ratingLabel.textColor = R.color.textMain()
            self.ratingLabel.text = R.string.localizable.withoutRating()
            return
        }
        
        var ratingColor: UIColor? = .clear
        
        switch self.model?.ratingGrade {
        case .high:
            ratingColor = R.color.highRating()
        case .medium:
            ratingColor = R.color.mediumRating()
        case .low:
            ratingColor = R.color.lowRating()
        default: break
        }
        
        let stringRating = R.string.localizable.ratingPrefix() + rating
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: stringRating)
        attributedString.cin.setColorForText(textForAttribute: R.string.localizable.ratingPrefix(),
                                             withColor: R.color.textMain(),
                                             withFont: R.font.graphikLCGRegular(size: .attributeFontSize))
        attributedString.cin.setColorForText(textForAttribute: rating,
                                             withColor: ratingColor,
                                             withFont: R.font.graphikLCGMedium(size: .ratingLabelFontSize))
        self.ratingLabel.attributedText = attributedString
    }
    
// MARK: - Show Skeleton
    private func showSkeleton() -> Bool {
        if self.model?.showSkeleton ?? false {
            self.containerView.backgroundColor = .clear
            self.localizedNameSkeletonView.isHidden = false
            self.ratingSkeletonView.isHidden = false
            self.nameSkeletonView.isHidden = false
            
            self.localizedNameSkeletonView.showAnimatedSkeleton()
            self.ratingSkeletonView.showAnimatedSkeleton()
            self.nameSkeletonView.showAnimatedSkeleton()
            return true
        }

        self.localizedNameSkeletonView.isHidden = true
        self.ratingSkeletonView.isHidden = true
        self.nameSkeletonView.isHidden = true
        
        self.containerView.backgroundColor = R.color.backgroundFilm()
        self.localizedNameSkeletonView.hideSkeleton()
        self.ratingSkeletonView.hideSkeleton()
        self.nameSkeletonView.hideSkeleton()
        return false
    }
}

// MARK: - CGFloat
fileprivate extension CGFloat {
    
    static var nameLabelFontSize: CGFloat = 14.0
    static var localizedNameLabelFontSize: CGFloat = 19.0
    static var ratingLabelFontSize: CGFloat = 15.0
    static var attributeFontSize: CGFloat = 13.0
    static var containerCornerRadius: CGFloat = 6.0
}

// MARK: - Float
fileprivate extension Float {

    static var skeletonCornerRadius: Float = 6.0
}
