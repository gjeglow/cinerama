//
//  FilmInfoModelRow.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 29.10.2021.
//
import UIKit

class FilmInfoModelRow: ModelRow {
    
    private(set) var localizedName: String?
    private(set) var name: String?
    private(set) var rating: Float?
    private(set) var ratingGrade: RatingGrade?
    
    convenience init(withFilm film: Film) {
        self.init(id: film.id)
        
        self.localizedName = film.localizedName
        self.name = film.name
        self.rating = film.rating
        self.ratingGrade = film.ratingGrade
    }
}
