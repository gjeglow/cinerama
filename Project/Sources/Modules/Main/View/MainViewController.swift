//
//  MainViewController.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 27.10.2021.
//

import UIKit
import SwiftUI

// MARK: - MainSection
private enum MainSection: Int {
    
    case films
    
    var id: String {
        return String(self.rawValue)
    }
    
    static func typeOf(id: String) -> MainSection? {
        guard let value = Int(id) else { return nil }
        return MainSection(rawValue: value)
    }
}

// MARK: - MainView
protocol MainView: BaseView {
    
// MARK: - Show data
    func show(data: MainData)
    func show(data: MainData, animated: Bool)
}

// MARK: - MainViewController
class MainViewController: BaseTableViewController {

// MARK: - VUPER
    var presenter: MainPresenter?
    var configurator: MainConfigurator?

// MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavigationBar()
        self.presenter?.viewDidLoad()
    }
    
// MARK: - Fabric
    override func setupFabric() {
        self.fabric = MainFabric(tableView: self.tableView, view: self)
    }
    
// MARK: - Refresh
    override func onRefreshControlChanged() {
        super.onRefreshControlChanged()
        
        self.presenter?.onRefreshControlChanged()
    }
}

// MARK: - VUPER Setup
extension MainViewController {

    func setup(presenter: MainPresenter?,
               configurator: MainConfigurator?) {
        self.presenter = presenter
        self.configurator = configurator
    }

}

extension MainViewController: MainView {
    
    private func configureNavigationBar() {
        self.navigationItem.title = R.string.localizable.films()
    }
    
// MARK: - Show data
    func show(data: MainData) {
        self.show(data: data, animated: true)
    }
    
    func show(data: MainData, animated: Bool) {
        self.configureRefreshControl()
        self.modelSections.removeAll()
        if data.films.isEmpty {
            self.modelSections.append(self.createSkeletonFilmsSection())
        } else {
            self.modelSections.append(self.createFilmsSection(data.films))
        }
        self.tableView.reloadData()
    }
    
// MARK: - Refresh Control
    private func configureRefreshControl() {
        if self.refreshControl.isRefreshing {
            self.onRefreshControlStopped()
        }
        self.setupRefreshControl()
    }

// MARK: - Create Films Section
    private func createFilmsSection(_ films: [Film]) -> ModelSection {
        let section = ModelSection(id: MainSection.films.id)
        var rows = [ModelRow]()

        let sortedFilms = films.sorted { f1, f2 in
            if f1.year != f2.year {
                return f1.year ?? Int.min < f2.year ?? Int.min
            } else {
                return f1.rating ?? Float.leastNormalMagnitude > f2.rating ?? Float.leastNormalMagnitude
            }
        }
        
        var lastAddedYear: Int?
        
        sortedFilms.forEach { film in
            
            if lastAddedYear != film.year {
                let yearModel = YearModelRow(withId: film.id,
                                             year: film.year)
                rows.append(yearModel)
                lastAddedYear = film.year
            }
            
            let filmModel = FilmInfoModelRow(withFilm: film)
            rows.append(filmModel)
            
        }
        
        section.rows = rows
        return section
    }
    
// MARK: - Create Films Section
    private func createSkeletonFilmsSection() -> ModelSection {
        let section = ModelSection(id: MainSection.films.id)
        var rows = [ModelRow]()
        
        for index in 0...2 {
            let yearModel = YearModelRow(id: "\(index)")
            yearModel.showSkeleton = true
            
            rows.append(yearModel)
            
            let filmInfoModelRow = FilmInfoModelRow(id: "\(index))")
            filmInfoModelRow.showSkeleton = true
            
            rows.append(filmInfoModelRow)
        }

        section.rows = rows
        return section
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        super.tableView(tableView, didSelectRowAt: indexPath)
                
        if let model = self.modelSections[indexPath.section].rows[indexPath.row] as? FilmInfoModelRow {
            self.presenter?.onFilmModelClicked(model.id)
        }
    }
}
