//
//  MainFabric.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 29.10.2021.
//

import UIKit

class MainFabric: ModelTableFabric {
    
// MARK: - Properties
    private weak var view: MainViewController?
    
    init(tableView: UITableView, view: MainViewController) {
        super.init(tableView: tableView)

        self.view = view
    }
    
    override func registerCells() {
        self.tableView?.cin.register(YearCell.self)
        self.tableView?.cin.register(FilmInfoCell.self)
    }
    
    override func cell(forModelRow modelRow: ModelRow,
                       atIndexPath indexPath: IndexPath) -> UITableViewCell {
        guard let tableView = self.tableView else { return UITableViewCell() }
        
        if let model = modelRow as? YearModelRow,
           let cell = tableView.cin.dequeueReusableCell(YearCell.self,
                                                        indexPath: indexPath) {
            cell.bind(withModel: model)
            return cell
        }
        
        if let model = modelRow as? FilmInfoModelRow,
           let cell = tableView.cin.dequeueReusableCell(FilmInfoCell.self,
                                                        indexPath: indexPath) {
            cell.bind(withModel: model)
            return cell
        }
        
        return UITableViewCell()
    }
}
