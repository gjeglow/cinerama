//
//  FilmCardConfigurator.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import UIKit

// MARK: - FilmCardConfigurator
protocol FilmCardConfigurator: BaseConfigurator {
    
    func setFilm(_ film: Film?)
}

// MARK: - FilmCardConfiguratorImpl
class FilmCardConfiguratorImpl: BaseConfiguratorImpl {

// MARK: - VUPER
    private weak var presenter: FilmCardPresenter?

// MARK: - Outlets
    @IBOutlet private weak var viewController: FilmCardViewController!
    
// MARK: - Configure
    override func configure() {
        let router = FilmCardRouterImpl(view: self.viewController)

        let useCases = FilmCardUseCases()

        let presenter = FilmCardPresenterImpl(view: self.viewController, router: router, useCases: useCases)
        self.presenter = presenter

        self.viewController.setup(presenter: self.presenter, configurator: self)
    }
}

extension FilmCardConfiguratorImpl: FilmCardConfigurator {

    func setFilm(_ film: Film?) {
        self.presenter?.setFilm(film)
    }
}

