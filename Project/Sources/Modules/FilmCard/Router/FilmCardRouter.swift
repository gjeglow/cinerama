//
//  FilmCardRouter.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import UIKit

// MARK: - FilmCardRouter
protocol FilmCardRouter: BaseRouter {

}

// MARK: - MainRouterImpl
class FilmCardRouterImpl: BaseRouterImpl {

// MARK: - VUPER
    private weak var view: FilmCardViewController?

// MARK: - Init
    required init(view: FilmCardViewController) {
        self.view = view
    }
}

// MARK: - FilmCardRouter
extension FilmCardRouterImpl: FilmCardRouter {

}
