//
//  FilmCardPresenter.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import Foundation

// MARK: - FilmCardPresenter
protocol FilmCardPresenter: BasePresenter {

    func setFilm(_ film: Film?)
}

// MARK: - FilmCardPresenterImpl
class FilmCardPresenterImpl: BasePresenterImpl {

// MARK: - VUPER
    private weak var view: FilmCardView?
    private var router: FilmCardRouter?

// MARK: - Data
    private var data = FilmCardData()

// MARK: - UseCases
    private var useCases: FilmCardUseCases

// MARK: - Init
    required init(view: FilmCardView,
                  router: FilmCardRouter,
                  useCases: FilmCardUseCases) {
        self.view = view
        self.router = router
        self.useCases = useCases
    }

// MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view?.show(data: self.data)
    }
}

// MARK: - FilmCardPresenter
extension FilmCardPresenterImpl: FilmCardPresenter {

    func setFilm(_ film: Film?) {
        self.data.film = film
    }
}

