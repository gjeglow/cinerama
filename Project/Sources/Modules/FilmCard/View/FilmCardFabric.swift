//
//  FilmCardFabric.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 30.10.2021.
//

import UIKit

class FilmCardFabric: ModelTableFabric {
    
// MARK: - Properties
    private weak var view: FilmCardViewController?
    
    init(tableView: UITableView, view: FilmCardViewController) {
        super.init(tableView: tableView)

        self.view = view
    }
    
    override func registerCells() {
        self.tableView?.cin.register(FilmCardCell.self)
    }
    
    override func cell(forModelRow modelRow: ModelRow,
                       atIndexPath indexPath: IndexPath) -> UITableViewCell {
        guard let tableView = self.tableView else { return UITableViewCell() }
        
        if let model = modelRow as? FilmCardModelRow,
           let cell = tableView.cin.dequeueReusableCell(FilmCardCell.self,
                                                        indexPath: indexPath) {
            cell.bind(withModel: model)
            return cell
        }
        
        return UITableViewCell()
    }
}
