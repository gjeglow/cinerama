//
//  FilmCardModelRow.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import Foundation

class FilmCardModelRow: ModelRow {
    
    private(set) var name: String?
    private(set) var year: Int?
    private(set) var rating: Float?
    private(set) var description: String?
    private(set) var imageUrl: String?
    private(set) var ratingGrade: RatingGrade?
    
    convenience init(withFilm film: Film) {
        self.init(id: film.id)
        
        self.name = film.name
        self.year = film.year
        self.rating = film.rating
        self.description = film.description
        self.imageUrl = film.imageUrl
        self.ratingGrade = film.ratingGrade
    }
}
