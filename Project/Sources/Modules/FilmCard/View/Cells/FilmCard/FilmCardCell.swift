//
//  FilmCardCell.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import UIKit
import Kingfisher

class FilmCardCell: UITableViewCell {
    
// MARK: - Properties
    private var model: FilmCardModelRow?
    
// MARK: - Outlets
    @IBOutlet weak private var filmImageView: UIImageView!
    @IBOutlet weak private var nameLabel: UILabel!
    @IBOutlet weak private var yearLabel: UILabel!
    @IBOutlet weak private var ratingLabel: UILabel!
    @IBOutlet weak private var descriptionLabel: UILabel!
    
// MARK: - awakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.nameLabel.font = R.font.graphikLCGMedium(size: .nameLabelFontSize)
        self.nameLabel.textColor = R.color.textMain()
        self.yearLabel.font = R.font.graphikLCGMedium(size: .yearLabelFontSize)
        self.ratingLabel.font = R.font.graphikLCGMedium(size: .ratingLabelFontSize)
        self.descriptionLabel.font = R.font.graphikLCGMedium(size: .descriptionLabelFontSize)
        self.descriptionLabel.textColor = R.color.textMain()
    }
    
// MARK: - Bind and configurate
    @discardableResult
    func bind(withModel model: FilmCardModelRow?) -> Self {
        self.model = model
        
        self.configure()
        return self
    }
    
    private func configure() {
        self.filmImageView.cin.setImage(fromURLString: self.model?.imageUrl,
                                        loader: true)

        self.nameLabel.text = self.model?.name
        self.yearLabel.text = self.model?.year?.description
        self.descriptionLabel.text = self.model?.description
        self.configureYear()
        self.configureRating()
    }
    
    private func configureYear() {
        guard let year = self.model?.year?.description else { return }
        
        let stringYear = R.string.localizable.yearPrefix() + year
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: stringYear)
        attributedString.cin.setColorForText(textForAttribute: R.string.localizable.yearPrefix(),
                                             withColor: R.color.textMain(),
                                             withFont: R.font.graphikLCGRegular(size: .attributeFontSize))
        attributedString.cin.setColorForText(textForAttribute: year,
                                             withColor: R.color.textMain(),
                                             withFont: R.font.graphikLCGMedium(size: .ratingLabelFontSize))
        self.yearLabel.attributedText = attributedString
    }
   
    private func configureRating() {
        guard let rating = self.model?.rating?.description else { return }
        
        var ratingColor: UIColor? = .clear
        
        switch self.model?.ratingGrade {
        case .high:
            ratingColor = R.color.highRating()
        case .medium:
            ratingColor = R.color.mediumRating()
        case .low:
            ratingColor = R.color.lowRating()
        default: break
        }
        
        let stringRating = R.string.localizable.ratingPrefix() + rating
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: stringRating)
        attributedString.cin.setColorForText(textForAttribute: R.string.localizable.ratingPrefix(),
                                             withColor: R.color.textMain() ,
                                             withFont: R.font.graphikLCGRegular(size: .attributeFontSize))
        attributedString.cin.setColorForText(textForAttribute: rating,
                                             withColor: ratingColor,
                                             withFont: R.font.graphikLCGMedium(size: .ratingLabelFontSize))
        self.ratingLabel.attributedText = attributedString
    }
}

// MARK: - CGFloat
fileprivate extension CGFloat {

    static var attributeFontSize: CGFloat = 13.0
    static var yearLabelFontSize: CGFloat = 15.0
    static var ratingLabelFontSize: CGFloat = 16.0
    static var nameLabelFontSize: CGFloat = 14.0
    static var descriptionLabelFontSize: CGFloat = 14.0
}

