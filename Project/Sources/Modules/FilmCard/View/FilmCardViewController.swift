//
//  FilmCardViewController.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import UIKit

// MARK: - FilmCardSection
private enum FilmCardSection: Int {
    
    case film
    
    var id: String {
        return String(self.rawValue)
    }
    
    static func typeOf(id: String) -> FilmCardSection? {
        guard let value = Int(id) else { return nil }
        return FilmCardSection(rawValue: value)
    }
}

// MARK: - FilmCardView
protocol FilmCardView: BaseView {
    
// MARK: - Show data
    func show(data: FilmCardData)
    func show(data: FilmCardData, animated: Bool)
}

// MARK: - FilmCardViewController
class FilmCardViewController: BaseTableViewController {

// MARK: - VUPER
    var presenter: FilmCardPresenter?
    var configurator: FilmCardConfigurator?

// MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.viewDidLoad()
    }
    
// MARK: - Fabric
    override func setupFabric() {
        self.fabric = FilmCardFabric(tableView: self.tableView, view: self)
    }
}

// MARK: - VUPER Setup
extension FilmCardViewController {

    func setup(presenter: FilmCardPresenter?,
               configurator: FilmCardConfigurator?) {
        self.presenter = presenter
        self.configurator = configurator
    }

}

extension FilmCardViewController: FilmCardView {
    
// MARK: - Show data
    func show(data: FilmCardData) {
        self.show(data: data, animated: true)
    }
    
    func show(data: FilmCardData, animated: Bool) {
        guard let film = data.film else { return }
        
        self.configureNavigationBar(withTitle: film.localizedName)
        self.modelSections.removeAll()
        self.modelSections.append(self.createFilmSection(film))
        self.tableView.reloadData()
    }
    
    private func configureNavigationBar(withTitle title: String?) {
        self.title = title
    }

// MARK: - Create Film Section
    private func createFilmSection(_ film: Film) -> ModelSection {
        let section = ModelSection(id: FilmCardSection.film.id)
        var rows = [ModelRow]()
        let filmCardModel = FilmCardModelRow(withFilm: film)
        rows.append(filmCardModel)
        section.rows = rows
        return section
    }
}
