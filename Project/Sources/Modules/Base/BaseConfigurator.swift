//
//  BaseConfigurator.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import Foundation

// MARK: - VUPER Configurator
public protocol BaseConfigurator where Self: NSObject {

// MARK: - Configuration
    func configure()
}

// MARK: - BaseConfiguratorImpl
class BaseConfiguratorImpl: NSObject, BaseConfigurator {

// MARK: - Live cycle
    override func awakeFromNib() {
        super.awakeFromNib()

        self.configure()
    }

// MARK: - Methods
    func configure() {

    }

}
