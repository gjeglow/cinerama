//
//  BaseRouter.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import UIKit

// MARK: - VUPER Router
public protocol BaseRouter: AnyObject {
    
// MARK: - Variables
    var topMostViewController: UIViewController? { get }
    var topMostNavigationController: UINavigationController? { get }
    var currentView: UIViewController? { get }
    
// MARK: - Navigation actions
    func setRootController(_ controller: UIViewController,
                           animated: Bool)
    func pop(animated: Bool)
    func popToRoot(animated: Bool)
    func dismiss(animated: Bool)
    func dismiss(animated: Bool, completion: (() -> Void)?)
}

// MARK: - BaseRouterImpl
class BaseRouterImpl: BaseRouter {
    
// MARK: - Variables
    var topMostViewController: UIViewController? {
        UIWindow.cin.topMostViewController
    }
    
    var topMostNavigationController: UINavigationController? {
        UIWindow.cin.currentNavigationController
    }
    
    var currentView: UIViewController? {
        UIWindow.cin.currentViewController
    }
    
// MARK: - Navigation actions
    func setRootController(_ controller: UIViewController, animated: Bool) {
        guard let window = UIWindow.cin.keyWindow else { return }
        
        if animated {
            window.setRootViewController(controller,
                                         options: UIWindow.TransitionOptions(direction: .fade))
        } else {
            window.rootViewController = controller
        }
    }
    
    func pop(animated: Bool) {
        self.topMostNavigationController?.popViewController(animated: animated)
    }
    
    func popToRoot(animated: Bool) {
        self.topMostNavigationController?.popToRootViewController(animated: animated)
    }
    
    func dismiss(animated: Bool) {
        self.dismiss(animated: animated, completion: nil)
    }
    
    func dismiss(animated: Bool, completion: (() -> Void)?) {
        self.topMostViewController?.dismiss(animated: animated, completion: completion)
    }

}
