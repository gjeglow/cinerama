//
//  BaseTableViewController.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import UIKit

// MARK: - BaseTableViewController
class BaseTableViewController: BaseViewController {

// MARK: - Properties
    var fabric: ModelTableFabric!
    var modelSections = [ModelSection]()
    var bottomInset: CGFloat = 0
    var refreshControl = UIRefreshControl()

    var headerHeightsDictionary: [Int: CGFloat] = [:]
    var cellHeightsDictionary: [IndexPath: CGFloat] = [:]
    var footerHeightsDictionary: [Int: CGFloat] = [:]

// MARK: - Outlets
    @IBOutlet internal weak var tableView: UITableView!

// MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupTableView()
    }
    
// MARK: - Loading indicator
    override func setLoadingVisible(_ visible: Bool) {
        DispatchQueue.main.async {
            if visible {
                self.baseLoaderContainer.center = self.tableView.center
                self.baseLoaderContainer.addSubview(self.baseLoaderView)
                self.view.addSubview(self.baseLoaderContainer)
                self.baseLoaderContainer.cin.rotate()
            } else {
                self.baseLoaderContainer.cin.stopRotating()
                self.baseLoaderContainer.removeFromSuperview()
            }
        }
    }
    
// MARK: - Setup table view
    func setupTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self

        self.tableView.separatorStyle = .none
        
        if self.tableView.style == .plain {
            self.tableView.tableHeaderView = UIView(frame: .zero)
            self.tableView.tableFooterView = UIView(frame: .zero)
        }
        
        self.setupFabric()
    }

    func setupFabric() {
        assertionFailure("not init fabric")
    }

// MARK: - Refresh control
    func setupRefreshControl() {
        self.refreshControl.tintColor = R.color.accentMain()
        self.refreshControl.addTarget(self, action: #selector(self.onRefreshControlChanged), for: .valueChanged)
        self.tableView.refreshControl = self.refreshControl
    }
    
    @objc
    func onRefreshControlChanged() {
        DispatchQueue.main.async {
            self.refreshControl.beginRefreshing()
        }
    }
    
    func onRefreshControlStopped() {
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
        }
    }
    
// MARK: - Sections
    public func getSection(byId id: String) -> ModelSection? {
        return self.modelSections.filter { $0.id == id }.first
    }
    
    public func removeSection(byId id: String) {
        self.modelSections.removeAll { $0.id == id }
    }
}

// MARK: - TableViewDataSource
extension BaseTableViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.modelSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.modelSections[section].headerModel?.expanded ?? true ? self.modelSections[section].rows.count : 0
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = self.modelSections[indexPath.section]
        let row = section.rows[indexPath.row]
        return self.fabric.cell(forModelRow: row,
                                atIndexPath: indexPath)
    }
}

// MARK: - TableViewDelegate
extension BaseTableViewController: UITableViewDelegate {

    
// MARK: - Section header
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        self.headerHeightsDictionary[section] = view.frame.height
    }

    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return self.headerHeightsDictionary[section] ?? 5
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.modelSections[section].headerModel == nil ? 0.01 : UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.fabric.header(forModelSection: self.modelSections[section].headerModel, atSection: section)
    }
    
// MARK: - Cell
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.cellHeightsDictionary[indexPath] = cell.frame.height
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.cellHeightsDictionary[indexPath] ?? 5
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

// MARK: - Section footer
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        self.footerHeightsDictionary[section] = view.bounds.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return self.footerHeightsDictionary[section] ?? 5
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {

        return self.modelSections[section].footerModel == nil ? 0.01 : UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return self.fabric.footer(forModelSection: self.modelSections[section].footerModel, atSection: section)
    }
}
