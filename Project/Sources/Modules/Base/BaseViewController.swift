//
//  BaseViewController.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import UIKit

// MARK: - VUPER View extensions
protocol BaseView: UIViewController {
    
// MARK: - Loading indicator
    func setLoadingVisible(_ visible: Bool)
    
// MARK: - Error message
    func showErrorMessage(_ message: String?)
    func showErrorMessage(_ message: String?,
                          retryBlock: @escaping () -> Void)
}

// MARK: - Base View Controller
class BaseViewController: UIViewController,
                          UIAdaptivePresentationControllerDelegate,
                          BaseView {
    
//    @IBOutlet weak var loadingIndicatorView: LoadView!
    
// MARK: - Properties
    internal let baseLoaderContainer = UIView(frame: CGRect(x: .zero, y: .zero, width: 36, height: 36))
    internal let baseLoaderView = UIImageView(image: R.image.imgLoaderOnImg())
    
// MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupBackButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    override var title: String? {
        get {
            return self.navigationItem.title
        }
        set {
            self.navigationController?.navigationBar.titleTextAttributes = [
                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .medium)
            ]
            self.navigationItem.title = newValue
        }
    }
  
// MARK: - Navigation bar
    internal func setupBackButtonItem() {
        let backArrowImage = R.image.icArrowLeftOutline()
        self.navigationController?.navigationBar.backIndicatorImage = UIImage()
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(image: backArrowImage, style: .plain, target: self, action: nil)
        self.navigationController?.navigationBar.tintColor = R.color.iconMain()
        self.navigationItem.backBarButtonItem?.imageInsets = UIEdgeInsets(top: .zero, left: 16, bottom: .zero, right: .zero)
    }

    internal func setupNavigationBar() {
    }

// MARK: - Actions
    @objc
    func onCloseButtonClicked() {
        self.dismiss(animated: true)
    }

// MARK: - VUPER View - Loading indicator
    func setLoadingVisible(_ visible: Bool) {
        if visible {
            DispatchQueue.main.async {
                self.baseLoaderContainer.center = self.view.center
                self.baseLoaderContainer.addSubview(self.baseLoaderView)
                self.view.addSubview(self.baseLoaderContainer)
                self.baseLoaderContainer.cin.rotate()
            }
        } else {
            DispatchQueue.main.async {
                self.baseLoaderContainer.cin.stopRotating()
                self.baseLoaderContainer.removeFromSuperview()
            }
        }
    }

// MARK: - Error message
    func showErrorMessage(_ message: String?) {
        self.showAlert(message,
                       title: R.string.localizable.somethingWentWrong())
    }
    
    func showErrorMessage(_ message: String?, retryBlock: @escaping () -> Void) {
        self.showAlert(message,
                       title: R.string.localizable.somethingWentWrong(),
                       leftButtonTitle: R.string.localizable.ok(),
                       leftButtonHandler: nil,
                       rightButtonTitle: R.string.localizable.refresh(),
                       rightButtonHandler: retryBlock)
    }
    
// MARK: - Alert
    func showAlert(_ message: String?,
                   title: String?,
                   leftButtonTitle: String? = nil,
                   leftButtonHandler: (() -> Void)? = nil,
                   rightButtonTitle: String? = nil,
                   rightButtonHandler: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if let leftTitle = leftButtonTitle {
            let style: UIAlertAction.Style = leftButtonHandler != nil ? .default : .cancel
            let action = UIAlertAction(title: leftTitle, style: style, handler: { (_) in
                leftButtonHandler?()
            })
            alert.addAction(action)
        }
        if let rightTitle = rightButtonTitle {
            let style: UIAlertAction.Style = rightButtonHandler != nil ? .default : .cancel
            let action = UIAlertAction(title: rightTitle, style: style, handler: { (_) in
                rightButtonHandler?()
            })
            alert.addAction(action)
        }
        if leftButtonHandler == nil && rightButtonHandler == nil && leftButtonTitle == nil && rightButtonTitle == nil {
            let action = UIAlertAction(title: R.string.localizable.ok(), style: .cancel, handler: nil)
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
// MARK: - Selectors
    @objc
    internal func searchItemTapped() {}
}

extension CGFloat {
    
    static let defaultBottomOffset: CGFloat = 16.0
    static let bottomButtonHeight: CGFloat = 70.0
}
