//
//  BasePresenter.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 28.10.2021.
//

import UIKit

// MARK: - VUPER Presenter
public protocol BasePresenter: AnyObject {

// MARK: - Life Cycle
    func viewDidLoad()
    func viewWillAppear()
    func viewDidAppear()
    func viewWillDisappear()
    func viewDidDisappear()
    
    func onCloseButtonClicked()
}

// MARK: - BasePresenterImpl
class BasePresenterImpl: BasePresenter {
  
// MARK: - Property
    internal lazy var operationsQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.underlyingQueue = .main
        return queue
    }()

// MARK: - Life Cycle
    func viewDidLoad() {}

    func viewWillAppear() {}

    func viewDidAppear() {}

    func viewWillDisappear() {}

    func viewDidDisappear() {}

    func viewDidUnload() {}

    func didReceiveMemoryWarning() {}
    
    func onCloseButtonClicked() {}
}
