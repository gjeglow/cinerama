//
//  ListFilmsUseCase.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 27.10.2021.
//

import SwiftyJSON
import Moya

protocol ListFilmsUseCase: AnyObject {
    
    func execute(completion: @escaping (_ success: Bool, _ films: [Film], _ message: String?, _ errorCode: String?) -> Void)
}

class ListFilmsUseCaseImpl: ListFilmsUseCase {
    
    let provider = MoyaProvider<FilmsProvider>(plugins: NetworkHelper.plugins)
    
    func execute(completion: @escaping (_ success: Bool, _ films: [Film], _ message: String?, _ errorCode: String?) -> Void) {
        self.provider.cin_executeJSONRequest(.loadFilms) { (success, json, errorMessage, errorCode) in
            
            if !success {
                completion(false, [], errorMessage ?? R.string.network.internalServerError(), errorCode)
                return
            }
            
            guard let filmsData = json?["films"] else {
                completion(false, [], errorMessage ?? R.string.network.unrecognizedResponse(), errorCode)
                return
            }
            
            let films = filmsData.array?.compactMap { Film(withJSON: $0) } ?? []
            
            completion(true, films, nil, nil)
        }
    }
}

