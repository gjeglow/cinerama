//
//  Film.swift
//  Cinerama
//
//  Created by Глеб Гагарин on 27.10.2021.
//

import SwiftyJSON

enum RatingGrade: String {
    
    case high
    case medium
    case low
}

class Film {
    
    private(set) var id: String!
    private(set) var localizedName: String?
    private(set) var name: String?
    private(set) var year: Int?
    private(set) var rating: Float?
    private(set) var description: String?
    private(set) var genres = [String]()
    private(set) var imageUrl: String?
    
    init?(withJSON json: JSON) {
        guard let id = json.cin.identifier() else { return nil }
        
        self.id = id
        self.name = json["name"].string
        self.localizedName = json["localized_name"].string
        self.year = json["year"].int
        self.rating = json["rating"].float
        self.description = json["description"].string
        self.genres = json["genres"].array?.compactMap { $0.string } ?? []
        self.imageUrl = json["image_url"].string
    }
}

extension Film {

    var ratingGrade: RatingGrade? {
        guard let rating = self.rating else { return nil }
        
        if rating >= 7.0 {
            return RatingGrade.high
        } else if rating < 5.0 {
            return RatingGrade.low
        } else {
            return RatingGrade.medium
        }
        
    }
}

