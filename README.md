## Небольшое описательное приложение для выбора фильмов на архитектуре VUPER

1. V - View Интерфейсный слой. Отвечает за отображение того что дает Presenter и за передачу введенной пользователем информации или действия в Presenter.
2. U - Usecase Логически выделенная атомарная операция, такая как загрузка данных с сети или базы данных.
3. P - Presenter Форматирование данных для отображения во View, реакция на ввод данных пользователем, получение данных из Usecase.
4. E - Entity Объект данных. Описание предметной модели без бизнес логики.
5. R - Router Слой описания логики навигации между экранами.

Поддержка пуш-уведомлений и отслеживание крашей через Firebase.

Поддержка App Tracking Transparency.

#### Скриншоты
- [`Сплешскрин`](https://gitlab.com/gjeglow/cinerama/-/raw/main/Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-11-20_at_15.20.15.png)
- [`Главная - список фильмов`](https://gitlab.com/gjeglow/cinerama/-/raw/main/Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-11-20_at_15.19.03.png)
- [`Карточка фильма`](https://gitlab.com/gjeglow/cinerama/-/raw/main/Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-11-20_at_15.19.36.png) 
- [`Обработка ошибок сети`](https://gitlab.com/gjeglow/cinerama/-/raw/main/Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-11-20_at_15.21.00.png) 
- [`Запрос на отображение Push уведомлений`](https://gitlab.com/gjeglow/cinerama/-/raw/main/Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-11-20_at_15.37.18.png) 
- [`Запрос на отслеживание`](https://gitlab.com/gjeglow/cinerama/-/raw/main/Simulator_Screen_Shot_-_iPhone_12_Pro_Max_-_2021-11-20_at_15.37.11.png)
 
